package com.ytzl.gotrip.rpc.api;

public interface RpcSendEmailService {
    /**
     * 发送邮件
     *
     * @param sender            发件人
     * @param receiver          收件人
     * @param title             主题
     * @param text              内容
     * @return
     */
    public String send(String sender,String receiver,String title,String text);
}
