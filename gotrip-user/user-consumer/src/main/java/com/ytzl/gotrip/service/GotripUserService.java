package com.ytzl.gotrip.service;

import com.ytzl.gotrip.model.GotripUser;
import com.ytzl.gotrip.utils.exception.GotripException;
import com.ytzl.gotrip.vo.userinfo.ItripUserVO;

/**
 * @author 76193
 */
public interface GotripUserService {

    /**
     * 根据登录账号查询用户信息
     * @param userCode
     * @return
     */
    public GotripUser findByUserCode(String userCode) throws Exception;

    /**
     * 通过手机号注册
     * @param itripUserVO  用户数据
     */
    public void registerByPhone(ItripUserVO itripUserVO) throws Exception;


    /**
     * 手机账号激活
     * @param user 登录账号
     * @param code 验证码
     */
    public void validatePhone(String user, String code) throws Exception;

    /**
     * 通过邮箱号注册
     *
     * @param itripUserVO   用户数据
     * @throws Exception
     */
    public void doregister(ItripUserVO itripUserVO) throws Exception;

    /**
     * 邮箱账号激活
     *
     * @param user  登录账号
     * @param code  验证码
     * @throws Exception
     */
    public void activate(String user, String code) throws Exception;

    /**
     * 用户名验证
     * @param userCode
     */
    public void ckusr(String userCode) throws Exception;


}
