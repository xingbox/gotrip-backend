package com.ytzl.gotrip.service.impl;

import com.alibaba.dubbo.config.annotation.Reference;
import com.ytzl.gotrip.ext.utils.RedisUtils;
import com.ytzl.gotrip.rpc.api.RpcGotripUserService;
import com.ytzl.gotrip.model.GotripUser;
import com.ytzl.gotrip.rpc.api.RpcSendEmailService;
import com.ytzl.gotrip.rpc.api.RpcSendMessageService;
import com.ytzl.gotrip.service.GotripUserService;
import com.ytzl.gotrip.utils.commons.Constants;
import com.ytzl.gotrip.utils.commons.DigestUtil;
import com.ytzl.gotrip.utils.commons.EmptyUtils;
import com.ytzl.gotrip.utils.commons.ErrorCode;
import com.ytzl.gotrip.utils.exception.GotripException;
import com.ytzl.gotrip.vo.userinfo.ItripUserVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * @author 76193
 */

@Service("gotripUserService")
public class GotripUserServiceImpl implements GotripUserService {

    private Logger LOG = LoggerFactory.getLogger(GotripUserServiceImpl.class);

    @Reference
    private RpcGotripUserService rpcGotripUserService;

    @Reference
    private RpcSendMessageService rpcSendMessageService;

    @Reference

    private RpcSendEmailService rpcSendEmailService;

    @Resource
    private RedisUtils redisUtils;

    @Override
    public GotripUser findByUserCode(String userCode) throws Exception {
        //校验数据
        if(EmptyUtils.isEmpty(userCode)){
            throw new GotripException("用户COde不能为空！", ErrorCode.AUTH_PARAMETER_ERROR);
        }
        Map<String,Object> params =new HashMap<>();
        params.put("userCode",userCode);
        List<GotripUser> gotripUserList = rpcGotripUserService.getGotripUserListByMap(params);
//        if(EmptyUtils.isEmpty(gotripUserList)){
//            throw new GotripException("登录账号不存在！", ErrorCode.AUTH_PARAMETER_ERROR);
//        }
        //return gotripUserList.get(0);
        return EmptyUtils.isEmpty(gotripUserList) ? null : gotripUserList.get(0);
    }

    @Override
    public void registerByPhone(ItripUserVO itripUserVO) throws Exception {
        //第一步 数据校验
        checkRegisterData(itripUserVO);
        //第二步 验证手机号
        if (!validPhone(itripUserVO.getUserCode())) {
            throw new GotripException("手机号格式不正确", ErrorCode.AUTH_PARAMETER_ERROR);
        }
        //第三步 判断用户是否存在
        GotripUser user = this.findByUserCode(itripUserVO.getUserCode());
        if (EmptyUtils.isNotEmpty(user)) {
            throw new GotripException("用户已存在", ErrorCode.AUTH_USER_ALREADY_EXISTS);
        }
        //第四步 构建用户信息
        GotripUser gotripUser = new GotripUser();
        BeanUtils.copyProperties(itripUserVO, gotripUser);
        //Activated 判断是否激活 0未激活
        gotripUser.setActivated(0);

        //密码加密
        String md5UserPassword = DigestUtil.hmacSign(gotripUser.getUserPassword());
        gotripUser.setUserPassword(md5UserPassword);

        //第五步 数据入库
        Integer resultSize = rpcGotripUserService.insertGotripUser(gotripUser);
        //第六步 发送手机验证码

        //需要构建四位的验证码
        int code = DigestUtil.randomCode();
        rpcSendMessageService.sendMassage(gotripUser.getUserCode(), "1", "" + code);
        //第七步 将验证码保存到redis中
        String key = Constants.RedisKeyPrefix.ACTIVATION_MOBILE_PREFIX + gotripUser.getUserCode();
        redisUtils.set(key, 60 * 3, "" + code);

    }

    @Override
    public void validatePhone(String user, String code) throws Exception {
        //验证手机号格式是否正确
        if (!validPhone(user)) {
            throw new GotripException("请输入正确的手机号！",
                    ErrorCode.AUTH_PARAMETER_ERROR);
        }

        //验证用户是否存在
        GotripUser gotripUser = this.findByUserCode(user);
        if (EmptyUtils.isEmpty(gotripUser)) {
            throw new GotripException("用户不存在！",
                    ErrorCode.AUTH_PARAMETER_ERROR);
        }

        //获取Redis中存储的短信验证码
        String key = Constants.RedisKeyPrefix.ACTIVATION_MOBILE_PREFIX + user;
        String cacheCode = (String) redisUtils.get(key);
        if (EmptyUtils.isEmpty(cacheCode) || !cacheCode.equals(code)) {
            throw new GotripException("验证码以失效！", ErrorCode.AUTH_PARAMETER_ERROR);
        }
        //激活用户
        gotripUser.setActivated(1);
        gotripUser.setUserType(0);
        gotripUser.setFlatID(gotripUser.getId());
        rpcGotripUserService.updateGotripUser(gotripUser);
        LOG.info("---->  用户[{}]激活成功",user);
    }

    @Override
    public void doregister(ItripUserVO itripUserVO) throws Exception {
        //第一步 数据校验
        checkRegisterData(itripUserVO);
        //第二步 验证手机号
        if (!validEmail(itripUserVO.getUserCode())) {
            throw new GotripException("邮箱格式不正确", ErrorCode.AUTH_PARAMETER_ERROR);
        }
        //第三步 判断用户是否存在
        GotripUser user = this.findByUserCode(itripUserVO.getUserCode());
        if (EmptyUtils.isNotEmpty(user)) {
            throw new GotripException("用户已存在", ErrorCode.AUTH_USER_ALREADY_EXISTS);
        }
        //第四步 构建用户信息
        GotripUser gotripUser = new GotripUser();
        BeanUtils.copyProperties(itripUserVO, gotripUser);
        //Activated 判断是否激活 0未激活
        gotripUser.setActivated(0);

        //密码加密
        String md5UserPassword = DigestUtil.hmacSign(gotripUser.getUserPassword());
        gotripUser.setUserPassword(md5UserPassword);

        //第五步 数据入库
        Integer resultSize = rpcGotripUserService.insertGotripUser(gotripUser);
        //第六步 发送手机验证码
        //需要构建四位的验证码
        int code = DigestUtil.randomCode();

        String sender="15136358062@163.com";   //这个是发送人的邮箱
        String receiver=gotripUser.getUserCode();  //这个是接受人的邮箱
        String title="欢迎您注册爱旅行，验证码是：";    //标题
        String text="您的验证码是："+code;
        String result= rpcSendEmailService.send(sender,receiver,title,text);
        //第七步 将验证码保存到redis中
        String key = Constants.RedisKeyPrefix.ACTIVATION_MOBILE_PREFIX + gotripUser.getUserCode();
        redisUtils.set(key, 60 * 3, "" + code);

    }

    @Override
    public void activate(String email, String code) throws Exception {
//验证邮箱格式是否正确
        if (!validEmail(email)) {
            throw new GotripException("请输入正确的邮箱！",
                    ErrorCode.AUTH_PARAMETER_ERROR);
        }

        //验证邮箱用户是否存在
        GotripUser gotripUser = this.findByUserCode(email);
        if (EmptyUtils.isEmpty(gotripUser)) {
            throw new GotripException("用户不存在！",
                    ErrorCode.AUTH_PARAMETER_ERROR);
        }

        //获取Redis中存储的验证码
        String key = Constants.RedisKeyPrefix.ACTIVATION_MOBILE_PREFIX + email;
        String cacheCode = (String) redisUtils.get(key);
        if (EmptyUtils.isEmpty(cacheCode) || !cacheCode.equals(code)) {
            throw new GotripException("验证码以失效！", ErrorCode.AUTH_PARAMETER_ERROR);
        }

        //激活用户
        gotripUser.setActivated(1);
        gotripUser.setUserType(0);
        gotripUser.setFlatID(gotripUser.getId());
        rpcGotripUserService.updateGotripUser(gotripUser);
        LOG.info("---->  用户[{}]激活成功", email);
    }

    @Override
    public void ckusr(String userCode) throws Exception {
        GotripUser user = this.findByUserCode(userCode);
        if(!EmptyUtils.isEmpty(user)){
            throw new GotripException("该账户已被注册",ErrorCode.AUTH_PARAMETER_ERROR);
        }
    }

    /**
     *  校验注册数据
     * @param itripUserVO 注册用户信息
     * @throws GotripException 如果为空抛出的异常
     */
    private void checkRegisterData(ItripUserVO itripUserVO) throws GotripException {
        if (EmptyUtils.isEmpty(itripUserVO)) {
            throw new GotripException("请传递参数", ErrorCode.AUTH_PARAMETER_ERROR);
        }
        if (EmptyUtils.isEmpty(itripUserVO.getUserCode())) {
            throw new GotripException("请传递用户账号", ErrorCode.AUTH_PARAMETER_ERROR);
        }
        if (EmptyUtils.isEmpty(itripUserVO.getUserName())) {
            throw new GotripException("请传递用户昵称", ErrorCode.AUTH_PARAMETER_ERROR);
        }
        if (EmptyUtils.isEmpty(itripUserVO.getUserPassword())) {
            throw new GotripException("请传递用户密码", ErrorCode.AUTH_PARAMETER_ERROR);
        }
    }

    /**			 *
     * 合法E-mail地址：
     * 1. 必须包含一个并且只有一个符号“@”
     * 2. 第一个字符不得是“@”或者“.”
     * 3. 不允许出现“@.”或者.@
     * 4. 结尾不得是字符“@”或者“.”
     * 5. 允许“@”前的字符中出现“＋”
     * 6. 不允许“＋”在最前面，或者“＋@”
     */
    private boolean validEmail(String email){

        String regex="^\\s*\\w+(?:\\.{0,1}[\\w-]+)*@[a-zA-Z0-9]+(?:[-.][a-zA-Z0-9]+)*\\.[a-zA-Z]+\\s*$"  ;
        return Pattern.compile(regex).matcher(email).find();
    }

    /**
     * 验证是否合法的手机号
     * @param phone
     * @return
     */
    private boolean validPhone(String phone) {
        String regex="^1[356789]{1}\\d{9}$";
        return Pattern.compile(regex).matcher(phone).find();
    }

}
